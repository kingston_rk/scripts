#!/bin/bash

set -e

while getopts "e:s:u:" arg; do
	case "${arg}" in
        s) sasToken=${OPTARG} ;;
		u) storageBaseUrl=${OPTARG} ;;
	esac
done

echo $sasToken
echo $storageBaseUrl

mkdir deployment
cd deployment

if ! systemctl is-active --quiet nginx 
    then
        echo "$(date +"%Y-%m-%d %T"): Install MongoDB" >> deployment_install.log
        # Install MongoDB
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
        sudo apt-get update
        sudo apt-get install -y mongodb-org unzip
        echo "$(date +"%Y-%m-%d %T"): MongoDB install complete" >> deployment_install.log
        
        sudo sed -i "s/127.0.0.1/0.0.0.0/" /etc/mongod.conf
        echo "$(date +"%Y-%m-%d %T"): MongoDB config complete" >> deployment_install.log
        
        sudo systemctl enable mongod && sudo systemctl start mongod
        echo "$(date +"%Y-%m-%d %T"): MongoDB enabled" >> deployment_install.log
fi

# Download data files and import into database
echo "$(date +"%Y-%m-%d %T"): Download data files starting" >> deployment_install.log

curl -sL "${storageBaseUrl}/import.sh${sasToken}" -o import.sh
echo "$(date +"%Y-%m-%d %T"): Download data files - import.json complete" >> deployment_install.log

curl -sL "${storageBaseUrl}/heroes.json${sasToken}" -o heroes.json
echo "$(date +"%Y-%m-%d %T"): Download data files - heroes.json complete" >> deployment_install.log

curl -sL "${storageBaseUrl}/ratings.json${sasToken}" -o ratings.json
echo "$(date +"%Y-%m-%d %T"): Download data files - ratings.json complete" >> deployment_install.log

curl -sL "${storageBaseUrl}/sites.json${sasToken}" -o sites.json
echo "$(date +"%Y-%m-%d %T"): Download data files - sites.json complete" >> deployment_install.log

echo "$(date +"%Y-%m-%d %T"): Download import script complete" >> deployment_install.log

echo "$(date +"%Y-%m-%d %T"): Data import starting" >> deployment_install.log
chmod +x ./import.sh
./import.sh

echo "$(date +"%Y-%m-%d %T"): Data import complete" >> deployment_install.log
